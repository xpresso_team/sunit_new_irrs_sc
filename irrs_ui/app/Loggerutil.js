'use strict'; //Basically it enables the Javascript strict mode
var winston = require('winston');

winston.configure({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({ filename: 'logs.txt' })
    ]
});

module.exports = winston;
